from dataclasses import dataclass, field
from abc import ABC, abstractmethod

from GameObject import GameObject, Vector2D
from Person import Person
from Warrior import Warrior
from Worker import Worker

@dataclass
class Build(GameObject):
    max_resistance: int = field(default=20)
    current_resistance: int = field(default=20)
    structures:GameObject = field(default=None)

    @abstractmethod
    def recruit_unit(self) -> Person:
        # crear_persona()
        pass


@dataclass
class Barrack(Build):

    def recruit_unit(self) -> Person:
        print("Ha nacido un nuevo soldado adulto listo para luchar")
        return Warrior()
    
    # crear_persona()


@dataclass
class CivicCenter(Build):

    def recruit_unit(self) -> Person:
        print("Ha nacido un nuevo aldeano adulo listo para trabajar")
        return Worker()

    # crear_persona()


@dataclass
class BuildDecorator(Build):
    wrapper:Build = field(default=None)


class NuclearReactor(BuildDecorator):
    def recruit_unit(self):
        print("El reactor nuclear no explotó y recluto un nuevo soldado")
        # return 


class Campfire(BuildDecorator):
    def recruit_unit(self):
        print("La fogata está encendida y se recluto un nuevo soldado")
        # return