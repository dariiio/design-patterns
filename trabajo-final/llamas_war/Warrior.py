from dataclasses import dataclass

from Person import Person


@dataclass
class Warrior(Person):
    
    def attack(self):
        print("¡El guerrero lanza golpes al aire infligiendo {} puntos de daño!".format(self.dagame))