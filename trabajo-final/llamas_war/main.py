from colorama import Fore, init

from Warrior import Warrior
from Worker import Worker
from Build import Barrack, CivicCenter, NuclearReactor, Campfire
from Flag import Flag

init()
# Probando a las clases hijas de Person

# Comenzamos con el warrior
print(Fore.RED + "Warrior!")
person_warrior = Warrior()
person_warrior.move((5,5))
person_warrior.attack()
person_warrior.stop()

# Ahora con el worker
print(Fore.YELLOW + "Worker!")
person_worker = Worker()
print(person_worker)
person_worker.move((10,6))
person_worker.attack()
print(person_worker)
person_worker.build()
person_worker.repair()
person_worker.stop()
person_worker.attack()
person_worker.cured()

# Probando flag (singleton)
print(Fore.CYAN + "A ver si anda el singleton...")
bandera_1 = Flag()
bandera_2 = Flag()

if id(bandera_1) == id(bandera_2):
    print("¡Son la misma instanciaa! weeeeeeee")

print(bandera_1)
bandera_2.change_owner("Jugador 1")
print(bandera_1)
bandera_2.change_owner("Jugador 2")


# Vamos a probar el metodo de factoreo... a noo no factory method
print(Fore.LIGHTMAGENTA_EX + "Construyamos edificios y probemos factory method!")

cuartel = Barrack()
new_warrior = cuartel.recruit_unit()
new_warrior.attack()
print(new_warrior)

centro_civico = CivicCenter()
new_aldeano = centro_civico.recruit_unit()
new_aldeano.attack()

# print(Fore.GREEN + "Probamos los decoradores")
# cuartel.structures = NuclearReactor()
# cuartel.structures.recruit_unit()
# cuartel.structures.wrapper = Campfire()
# cuartel.structures.wrapper.recruit_unit()
