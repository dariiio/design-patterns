from dataclasses import dataclass, field
from abc import ABC, abstractmethod



@dataclass
class StatePerson(ABC):

    def __init__(self, person):
        self.person = person

    @abstractmethod
    def damaged(self):
        pass

    @abstractmethod
    def cured(self):
        pass

    @abstractmethod
    def fatally_damaged(self):
        pass


@dataclass
class Healthy(StatePerson):

    def __init__(self, person):
        super().__init__(person)

    def damaged(self):
        print("***STATE**** Esta persona se lastimó!")
        self.person.current_state = self.person.injured_state

    def cured(self):
        print(
            "***STATE**** A excepción del daño emocional, la persona se encuentra completamente sana!"
        )

    def fatally_damaged(self):
        print("***STATE**** Esta persona hizo la murición :(")
        self.person.current_state = self.person.dead_state


@dataclass
class Injured(StatePerson):

    def __init__(self, person):
        super().__init__(person)

    def damaged(self):
        print("***STATE**** Esta persona se lastimó!")
        self.person.current_state = self.person.injured_state

    def cured(self):
        print("***STATE**** Esta persona se curó")
        self.person.current_state = self.person.healthy_state

    def fatally_damaged(self):
        print("***STATE**** Esta persona hizo la murición :(")
        self.person.current_state = self.person.dead_state


@dataclass
class Dead(StatePerson):

    def __init__(self, person):
        super().__init__(person)

    def damaged(self):
        print("***STATE**** Esta persona ya no puede recibir mas daño...")

    def cured(self):
        print("***STATE**** Esta persona ya no puede curarse...")

    def fatally_damaged(self):
        print("***STATE**** Esta persona no puede morir de nuevo... al menos no en este plano")
