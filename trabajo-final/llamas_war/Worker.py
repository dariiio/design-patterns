from dataclasses import dataclass, field

from Person import Person


@dataclass
class Worker(Person):

    def __init__(self):
        super().__init__()

    def attack(self):
        # pasar al estado
        self.current_life -= self.dagame
        if self.current_life <= 0:
            self.current_state.fatally_damaged()
        else:
            self.current_state.damaged()
        print("El aldeano ataca con un martillo y se autolesiona... -{} de vida".format(self.dagame))

    def cured(self):
        self.current_life = self.max_life
        self.current_state.cured()

    def build(self):
        print("El aldeano se puso a martillar de forma aleatoria")

    def repair(self):
        print("El aldeano pretende apagar el fuego a martillazos")
