from dataclasses import dataclass, field
from abc import ABC, abstractmethod

from GameObject import GameObject, Vector2D
from StatePerson import StatePerson, Healthy, Injured, Dead


@dataclass
class Person(GameObject):
    max_life: int = field(default=10)
    current_life: int = field(default=10)
    armor: int = field(default=2)
    dagame: int = field(default=1)
    current_state: StatePerson = field(default=None)
    healthy_state: StatePerson = field(default=None)
    injured_state: StatePerson = field(default=None)
    dead_state: StatePerson = field(default=None)


    def __init__(self):
        self.healthy_state = Healthy(self)
        self.injured_state = Injured(self)
        self.dead_state = Dead(self)
        self.current_state = self.healthy_state

    def move(self, new_position: Vector2D = Vector2D()):
        self.position = new_position
        print("Esta persona se esta moviendo a {}".format(new_position))

    def stop(self):
        print("¡Esta persona se quedó completamente inmovil en una posición incomoda!")

    @abstractmethod
    def attack(self):
        pass
