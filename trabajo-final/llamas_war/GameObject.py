from dataclasses import dataclass, field
from abc import ABC


@dataclass
class Vector2D():
    x: float = field(default=0)
    y: float = field(default=0)

    def __repr__(self):
        return f'({self.x}, {self.y})'

@dataclass
class GameObject(ABC):
    position: Vector2D = field(default=Vector2D())
    sprite: str = field(default="llamita")