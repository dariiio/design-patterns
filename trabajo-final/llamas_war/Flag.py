from dataclasses import dataclass, field
from abc import ABC, abstractmethod

from GameObject import GameObject, Vector2D


class SingletonMeta(type):

    _instances = {}

    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            instance = super().__call__(*args, **kwargs)
            cls._instances[cls] = instance
        return cls._instances[cls]


@dataclass
class Flag(metaclass=SingletonMeta):
    owner: str = field(default='Nadie')

    def change_owner(self, new_owner):
        print("¡Chau {0}!... ¡¡ahora soy propiedad de {1}!!".format(self.owner, new_owner))
        self.owner = new_owner

    def __repr__(self) -> str:
        return "mi dueño es {}".format(self.owner)