from file_manager.FileManager import FileManager


class Directory(FileManager):

    content: FileManager = []

    def add(self, file: FileManager):
        self.content.append(file)
        print("Se agrego el archivo", file.get_name())

    def delete(self, file_name: str):
        print("Se borro el archivo")

    def get_size(self):
        s: int = 32
        for f in self.content:
            s += f.get_size()
        print("El tamaño es ", s)

    def file_copy(self):
        print("Se copio el directorio")

    def file_delete(self):
        print("Se borro el directorio")

    def file_open(self):
        print("Se abrio el directorio")
