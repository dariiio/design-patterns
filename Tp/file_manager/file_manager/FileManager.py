from abc import ABC, abstractmethod


class FileManager(ABC):
    file_name: str
    file_size: str
    file_path: str

    def __init__(self) -> None:
        super().__init__()

    def get_name(self) -> str:
        return self.file_name

    def rename(self, file_name: str) -> None:
        self.file_name = file_name

    @abstractmethod
    def get_size(self):
        pass

    @abstractmethod
    def file_copy(self):
        pass

    @abstractmethod
    def file_delete(self):
        pass

    @abstractmethod
    def file_open(self):
        pass
