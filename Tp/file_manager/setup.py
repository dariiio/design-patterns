import setuptools


# pip install -e file_manager
setuptools.setup(
    name="file-manager",
    version="0.0.1",
    description="A small package to practice design patterns",
    packages=setuptools.find_packages(),
    install_requires=[],
    python_requires=">=3.7",
)